#!/bin/bash

# File job_spec has following format:
# job_num;nodes;tasks
touch job_spec.csv

# First run the sequential version
srun --ntasks=1 ./sequential.out > slurm-seq.out

for nodes in {1,4}
do
        for i in {2,4,8,16,64,128}
        do

                JOB_ID=$( sbatch --ntasks=${i}  --nodes=${nodes}  job.sh | awk '{print $4}' )
                echo "${JOB_ID};${nodes};${i}" >> job_spec.csv

        done
done
echo "Processed all the jobs."
