#include "mpi.h"
#include <stdlib.h>


int main(int argc, char **argv){
  // before initializing is already printed by each desired thread
  printf("Grüß Gott\n");
  MPI_Init(&argc, &argv);
  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // now let's create a new communicator on each thread
  MPI_Comm new_comm;
  // color indicated how many threads will belong to the new communicator
  int color = rank/3;
  int key = abs(size - rank);

  MPI_Comm_split(MPI_COMM_WORLD, color, rank, &new_comm);

  // let's see the size and rank of the communicator now
  int new_size, new_rank;
  MPI_Comm_rank(new_comm, &new_rank);
  MPI_Comm_size(new_comm, &new_size);

  printf("AFTER: process %i out of %i, BEFORE: process %i out of %i; color: %i, key: %i\n", new_rank, new_size, rank, size, color, key);

  MPI_Finalize();
  return 0;
}

/**
Output when running on 8 threads:

Grüß Gott
...
Grüß Gott
AFTER: process 0 out of 3, BEFORE: process 0 out of 8; color: 0, key: 8
AFTER: process 1 out of 3, BEFORE: process 1 out of 8; color: 0, key: 7
AFTER: process 2 out of 3, BEFORE: process 2 out of 8; color: 0, key: 6
AFTER: process 0 out of 3, BEFORE: process 3 out of 8; color: 1, key: 5
AFTER: process 1 out of 3, BEFORE: process 4 out of 8; color: 1, key: 4
AFTER: process 2 out of 3, BEFORE: process 5 out of 8; color: 1, key: 3
AFTER: process 0 out of 2, BEFORE: process 6 out of 8; color: 2, key: 2
AFTER: process 1 out of 2, BEFORE: process 7 out of 8; color: 2, key: 1

**/
