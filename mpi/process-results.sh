#!/bin/bash

echo "parallel">results.csv
echo "job_id;nodes;tasks;gen time;conv time" >>results.csv

while IFS= read -r line
do
  JOB_NUM=$( echo ${line} | awk -F  ";" '{print $1}')
  GEN_TIME=$( grep "G>" *-${JOB_NUM}.out | awk -F '>' '{print $2}')
  CONV_TIME=$( grep "C>" *-${JOB_NUM}.out | awk -F '>' '{print $2}')

  echo "${line};${GEN_TIME};${CONV_TIME}" >> results.csv
done < "job_spec.csv"

echo -e "\nsequential" >> results.csv
echo "gen time; conv time" >> results.csv

GEN_TIME=$( grep "G>" slurm-seq.out | awk -F '>' '{print $2}')
CONV_TIME=$( grep "C>" slurm-seq.out | awk -F '>' '{print $2}')
