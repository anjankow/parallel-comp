#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>
#include <time.h>
#include <cmath>
#include <complex>
#include <chrono>
#include "mpi.h"

#include "a3-helpers.hpp"

#define TAG_GENERATE 88
#define TAG_CONVOLUTE 99

#define GENERATE_HEIGHT_OFFSET 0
#define GENERATE_WIDTH_OFFSET 1
#define GENERATE_PIXEL_OFFSET 2

using namespace std;

int world_rank;
int world_size;


// A set of random gradients, adjusted for this mandelbrot algorithm
vector<gradient> gradients = {
    gradient({0, 0, 0}, {76, 57, 125}, 0.0, 0.010, 2000),
    gradient({76, 57, 125}, {255, 255, 255}, 0.010, 0.020, 2000),
    gradient({255, 255, 255}, {0, 0, 0}, 0.020, 0.050, 2000),
    gradient({0, 0, 0}, {0, 0, 0}, 0.050, 1.0, 2000)};

    // Test if point c belongs to the Mandelbrot set
    bool mandelbrot_kernel(complex<double> c, vector<int> &pixel)
    {
        int max_iterations = 2048, iteration = 0;
        complex<double> z(0, 0);

        while (abs(z) <= 4 && (iteration < max_iterations))
        {
            z = z * z + c;
            iteration++;
        }

        double length = sqrt(z.real() * z.real() + z.imag() * z.imag());
        long double m = (iteration + 1 - log(length) / log(2.0));
        double q = m / (double)max_iterations;

        q = iteration + 1 - log(log(length)) / log(2.0);
        q /= max_iterations;

        colorize(pixel, q, iteration, gradients);

        return true;
    }

    /**
     * Compute the Mandelbrot set for each pixel of a given image.
     * Image is the Image data structure for storing RGB image
     * The default value for ratio is 0.15.
     *
     * @param[inout] image
     * @param[in] ratio
     *
     */
    int mandelbrot(Image &image, double ratio = 0.15)
    {
        int h = image.height;
        int w = image.width;
        int channels = image.channels;
        ratio /= 10.0;

        int pixels_inside=0;

        // pixel to be passed to the mandelbrot function
        vector<int> pixel = {0, 0, 0}; // red, green, blue (each range 0-255)
        complex<double> c;

        // first process only fills the image, so number of working processes is -1
        int step_size = world_size - 1;
        int total_num_of_messages = ((image.height/step_size) * (world_size - 1) + image.height%step_size) * image.width;
        // each pixel is written on following bytes:
        // B0: height
        // B1: width
        // B2: channel 0
        // ..
        // Bn+1: channel n-1
        vector<int>message = vector<int>(GENERATE_PIXEL_OFFSET + image.channels);

        if (world_rank == 0) {
            // printf("(%i) Receiving %i messages\n", world_rank, total_num_of_messages);

            for (int i = 0; i < total_num_of_messages; ++i) {
                MPI_Status status;
                MPI_Recv(message.data(), message.size(), MPI_INT, MPI_ANY_SOURCE, TAG_GENERATE, MPI_COMM_WORLD, &status);
                // printf("(%i) Received, i=%i, source=%i\n", world_rank, i, status.MPI_SOURCE);

                int h = message[GENERATE_HEIGHT_OFFSET];
                int w = message[GENERATE_WIDTH_OFFSET];
                for (int ch = 0; ch < image.channels; ++ch) {
                    image(ch, h, w) = message[GENERATE_PIXEL_OFFSET + ch];
                }
            }
        } else {

            int messages_sent = 0;
            for (int j = world_rank - 1; j < h; j+=step_size)
            {
                for (int i = 0; i < w; i++)
                {
                    double dx = (double)i / (w)*ratio - 1.10;
                    double dy = (double)j / (h)*0.1 - 0.35;

                    c = complex<double>(dx, dy);

                    if (mandelbrot_kernel(c, pixel)) // the actual mandelbrot kernel
                        pixels_inside++;

                    message[GENERATE_HEIGHT_OFFSET] = j;
                    message[GENERATE_WIDTH_OFFSET] = i;
                    for (int ch = 0; ch < channels; ch++)
                        message[GENERATE_PIXEL_OFFSET+ch] = pixel[ch];
                    // apply to the image, send a message
                    MPI_Send(message.data(), message.size(), MPI_INT, 0, TAG_GENERATE, MPI_COMM_WORLD);
                    messages_sent++;
                }
            }
            // printf("(%i) Sent %i generation messages\n", world_rank, messages_sent);

        }
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast(image.data.data(), image.data.size(), MPI_UNSIGNED, 0, MPI_COMM_WORLD);
        // printf("(%i) Generation done, image[x][y]=(%i %i %i)\n", world_rank, image(0,32,330), image(2,32,330),image(2,432,330));

        int global_pixels_inside = 0;
        MPI_Reduce(&pixels_inside, &global_pixels_inside, 1, MPI_INT, MPI_SUM, 0,
                   MPI_COMM_WORLD);

        return global_pixels_inside;
    }

    /**
     * 2D Convolution
     * src is the source Image to which we apply the filter.
     * Resulting image is saved in dst. The size of the kernel is
     * given with kernel_width (must be odd number). Sigma represents
     * the standard deviation of the filter. The number of iterations
     * is given with the nstep (default=1)
     *
     * @param[in] src
     * @param[out] dst
     * @param[in] kernel_width
     * @param[in] sigma
     * @param[in] nsteps
     *
     */
    void convolution_2d(Image &src, Image &dst, int kernel_width, double sigma, int nsteps=1)
    {
        int h = src.height;
        int w = src.width;
        int channels = src.channels;

        std::vector<std::vector<double>> kernel = get_2d_kernel(kernel_width, kernel_width, sigma);

        int displ = (kernel.size() / 2); // height==width!

        // vector<int> global_data = vector<int>(world_size);
        // vector<MPI_Request> requests = vector<MPI_Request>(world_size-1);
        int chunk;
        vector<int>sendcounts;
        vector<int>send_offset = vector<int>(world_size);
        if (w%world_size) {
            // image width is not divisible by number of threads
            chunk = w/world_size + 1;
            sendcounts = vector<int>(world_size, chunk);
            sendcounts[world_size - 1] = w - (world_size - 1)*chunk;

        } else {
            chunk = w/world_size;
            sendcounts = vector<int>(world_size, chunk);
        }
        for (int i = 0; i < world_size; ++i) {
            send_offset[i] = i * chunk;
        }
        // if (world_rank == 0) {
        //     printf("(%i) Chunk[0]=%i, chunk[n-1]=%i\n", world_rank, sendcounts[0], sendcounts[world_size-1]);
        // }




        for (int step = 0; step < nsteps; step++)
        {
            for (int ch = 0; ch < channels; ch++)
            {
                for (int i = 0; i < h; i++)
                {
                    int send_count = sendcounts[world_rank];
                    vector<unsigned int> send_buffer = vector<unsigned int>(send_count);

                    int jstart = send_offset[world_rank];

                    for (int j = jstart; j < jstart + send_count; ++j)
                    {
                        double val = 0.0;

                        for (int k = -displ; k <= displ; k++)
                        {
                            for (int l = -displ; l <= displ; l++)
                            {
                                int cy = i + k;
                                int cx = j + l;
                                int src_val = 0;

                                // if it goes outside we disregard that value
                                if (cx < 0 || cx > w - 1 || cy < 0 || cy > h - 1) {
                                    continue;
                                } else {
                                    src_val = src(ch, cy, cx);
                                }

                                val += kernel[k + displ][l + displ] * src_val;
                            }
                        }
                        send_buffer[j-jstart] = (unsigned int)(val > 255 ? 255 : (val < 0 ? 0 : val));

                    }
                    MPI_Gatherv(send_buffer.data(), sendcounts[world_rank], MPI_UNSIGNED, &dst(ch, i, 0), sendcounts.data(), send_offset.data(), MPI_UNSIGNED, 0, MPI_COMM_WORLD);
                    // now in root process (0) the destination image is filled

                }
                // if (world_rank == 0) {
                //     printf("(%i) Served ch=%i, step %i/%i\n", world_rank, ch, step, nsteps);
                //
                // }
            }

            if ( step < nsteps-1 ) {
                if (world_rank == 0) {

                    // swap references
                    // we can reuse the src buffer for this example
                    Image tmp = src; src = dst; dst = tmp;
                }

                // broadcast the updated source image
                MPI_Bcast(src.data.data(), src.data.size(), MPI_UNSIGNED, 0, MPI_COMM_WORLD);

            }

        }
    }

    int main(int argc, char **argv)
    {
        MPI_Init(&argc, &argv);

        MPI_Comm communicator = MPI_COMM_WORLD;
        MPI_Comm_rank(communicator, &world_rank);
        MPI_Comm_size(communicator, &world_size);
        // if (world_rank == 0) {
        //     printf("Total np: %i\n", world_size);
        // }

        // height and width of the output image
        // keep the height/width ratio for the same image
        int width = 1200, height = 800;
        double ratio = width / (double)height;

        int pixels_inside = 0;

        int channels = 3; // red, green, blue

        // Generate Mandelbrot set int this image
        Image image(channels, height, width);

        // Save the results of 2D convolution in this image
        Image filtered_image(channels, height, width);

        auto t1 = MPI_Wtime();

        // Generate the mandelbrot set
        // adjust sizes according to your data layout
        pixels_inside = mandelbrot(image, ratio);
        MPI_Barrier(MPI_COMM_WORLD);

        auto t2 = MPI_Wtime();

        // if (world_rank == 0) {
        //     // save image
        //     // you need to save data in a safe way
        //     std::ofstream ofs("mandelbrot_original.ppm", std::ofstream::out);
        //     ofs << "P3" << std::endl;
        //     ofs << width << " " << height << std::endl;
        //     ofs << 255 << std::endl;
        //
        //     for (int j = 0; j < height; j++)
        //     {
        //         for (int i = 0; i < width; i++)
        //         {
        //             ofs << " " << image(0, j, i) << " " << image(1, j, i) << " " << image(2, j, i) << std::endl;
        //         }
        //     }
        //     ofs.close();
        // }

        if (world_rank == 0) {
            // PRINT FOR RESULTS ANALYSIS
            printf("G>%f\n", t2-t1);

            // printf("(%i) Image generation time (mandelbrot): %f\n", world_rank, (t2 - t1));
            // cout << "Total Mandelbrot pixels: " << pixels_inside << endl;
        }

        // Actual 3D convolution part
        // use MPI to implement a version for distributed system

        auto t3 = MPI_Wtime();

        convolution_2d(image, filtered_image, 5, 0.37, 20);
        MPI_Barrier(MPI_COMM_WORLD);

        auto t4 = MPI_Wtime();

        if (world_rank == 0) {
            // PRINT FOR RESULTS ANALYSIS
            printf("C>%f\n", t4-t3);

            // cout << "Convolution time: " << (t4 - t3) << endl;
        }

        if (world_rank == 0) {
            // save image
            // you need to save data in a safe way
            std::ofstream ofs("mandelbrot.ppm", std::ofstream::out);
            ofs << "P3" << std::endl;
            ofs << width << " " << height << std::endl;
            ofs << 255 << std::endl;

            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    ofs << " " << filtered_image(0, j, i) << " " << filtered_image(1, j, i) << " " << filtered_image(2, j, i) << std::endl;
                }
            }
            ofs.close();
        }

        MPI_Finalize();

        return 0;
    }
