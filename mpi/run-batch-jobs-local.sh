#!/bin/bash

# File job_spec has following format:
# job_num;nodes;tasks
touch job_spec.csv

# First run the sequential version
echo "sequential"
./sequential.out > slurm-seq.out


nodes=1
JOB_ID=1
for i in {2,4,8,16,32}
do
  echo "i=${i}, job_id=${JOB_ID}"
  mpirun -np ${i} ./mandelbrot.out > local-${JOB_ID}.out
  cat local-${JOB_ID}.out
  echo "${JOB_ID};${nodes};${i}" >> job_spec.csv
  let JOB_ID=JOB_ID+1

done

echo "Processed all the jobs."
