# How to use scripts

To run the jobs:
```
./run-batch-jobs.sh
```

To process the results to a CSV table:
```
./process-results.sh
```

---
Files a3-sequential.cpp and a3-helpers.cpp were given - are not created by me.
