#include "mpi.h"
#include <stdlib.h>

int main(int argc, char **argv){

  MPI_Init(&argc, &argv);
  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank % 2) {
    // let half of the thread produce a message
    int dst = rank - 1;
    MPI_Send(&rank, sizeof(rank), MPI_INT, dst, 0, MPI_COMM_WORLD);
    printf("Sent to %i, rank: %i\n", dst, rank);
  } else {
    // the other half is gonna receive the message
    int message;
    MPI_Status status;
    int source = (rank + 1)%size;
    // printf("Receiving from %i, rank: %i\n", source, rank);
    MPI_Recv(&message, sizeof(message), MPI_INT, source, 0, MPI_COMM_WORLD, &status);
    printf("Message received from %i, rank: %i\n", message, rank);
  }

  MPI_Finalize();
  return 0;
}

/**
Output:

Message received from 1, rank: 0
Sent to 0, rank: 1
Message received from 3, rank: 2
Sent to 2, rank: 3
Message received from 5, rank: 4
Sent to 4, rank: 5
Message received from 7, rank: 6
Sent to 6, rank: 7
**/
