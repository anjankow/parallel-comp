#!/bin/bash

mpirun -np ${SLURM_NTASKS} --map-by node ./mandelbrot.out
