#include "mpi.h"
#include <stdlib.h>
#include <string>
#include <sstream>
#include <cstring>

#define MESSAGE_NUM 14
#define TAG_RESULTS 99

/**
There are two sections in this program.
The first one produces the messages.
Some of them are to be received in the first section, the remaining ones are received in the 'else' section.

The `message` array holds the information which thread received which message - so its rank.
The messages are sent with tag corresponding to the index in `message` array.

After receiving in the 'else' section, the thread sends the results back.
The first section is the one who publishes the results.

CONSTRAINT: works only if the number of messages < number of threads!
**/


void print_message(int message[MESSAGE_NUM], int rank){
  std::ostringstream stringStream;

  stringStream << "(" << rank << ") Message: [";
  for (int i = 0; i < MESSAGE_NUM; ++i){
    if (i != MESSAGE_NUM - 1){
      stringStream << message[i] << ", ";
    } else {
      stringStream << message[i] << "]\n";
    }
  }
  std::string toPrint = stringStream.str();
  printf("%s", toPrint.c_str());
}

int main(int argc, char **argv){

  MPI_Init(&argc, &argv);
  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int message[MESSAGE_NUM];
  memset(message, -1, MESSAGE_NUM * sizeof(int));

  if (!(rank % 2)) {

    for (int i = 0; i < MESSAGE_NUM; ++i) {
      // every second send is going to be received in this section
      int dst = (rank + i + 1)%size;
      // the tag is equal i and it's going to be interpreted as an index in the message array
      MPI_Send(&rank, sizeof(rank), MPI_INT, dst, i, MPI_COMM_WORLD);
      printf("(%i) Sent tag %i to %i\n", rank, i, dst);
    }

    // now let's receive the messages sent to this section
    int numMsgToRecv = MESSAGE_NUM/2;

    // number of results from the other section to be received
    int numOfResults = MESSAGE_NUM/2 + (MESSAGE_NUM % 2);

    int *results = new int[numOfResults];
    MPI_Status *statuses = new MPI_Status[numOfResults];
    MPI_Request *requests = new MPI_Request[numOfResults];

    int tagInit = 1;
    for (int i = 0; i < numMsgToRecv; ++i) {
      // tag is the message index
      int source = (size + rank - 2 - i * 2 )%size;
      int tag = tagInit + (2 * i)%MESSAGE_NUM;
      printf("(%i) Receiving from source %i tag %i\n", rank, source, tag);
      MPI_Irecv(&results[i], sizeof(int), MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &requests[i]);
    }
    MPI_Waitall(numMsgToRecv, requests, statuses);
    printf("(%i) Received all results from this section\n", rank);
    for (int i = 0; i < numOfResults; ++i) {

      message[statuses[i].MPI_TAG] = statuses[i].MPI_SOURCE;
    }


    // now let's fill the 'messages' with the data from the threads from the else part
    for (int i = 0; i < numOfResults; ++i) {
      MPI_Irecv(&results[i], sizeof(int), MPI_INT, MPI_ANY_SOURCE, TAG_RESULTS, MPI_COMM_WORLD, &requests[i]);
    }
    MPI_Waitall(numOfResults, requests, statuses);
    printf("(%i) All results received\n", rank);

    for (int i = 0; i < numOfResults; ++i) {
      // the value of the received message is the tag sent before, so the index in the messages array
      // the source received also the initial message, so its rank is the value in the messages array

      message[results[i]] = statuses[i].MPI_SOURCE;
    }
    delete [] requests;
    delete [] statuses;
    delete [] results;

    print_message(message, rank);

  } else {
    // when number of messages is even, the messages are spead equally among the two sections
    // otherwise this section has one more message more to receive
    int numMsgToRecv = MESSAGE_NUM/2 + (MESSAGE_NUM % 2);

    int *received_m = new int[numMsgToRecv];
    MPI_Status *statuses = new MPI_Status[numMsgToRecv];
    MPI_Request *requests = new MPI_Request[numMsgToRecv];

    for (int i = 0; i < numMsgToRecv; ++i){
      MPI_Irecv(&received_m[i], sizeof(int), MPI_INT, MPI_ANY_SOURCE, i * 2, MPI_COMM_WORLD, &requests[i]);
    }

    MPI_Waitall(numMsgToRecv, requests, statuses);
    for (int i = 0; i < numMsgToRecv; ++i) {
      printf("(%i) Received from (%i) tag %i\n", rank, statuses[i].MPI_SOURCE, statuses[i].MPI_TAG);
    }

    // now send the result to the other threads
    for (int i = 0; i < numMsgToRecv; ++i) {
      int value = statuses[i].MPI_TAG;
      int dst = statuses[i].MPI_SOURCE;
      printf("(%i) Sending result %i to %i\n", rank, value, dst);
      MPI_Isend(&value, sizeof(statuses[i].MPI_TAG), MPI_INT, dst, TAG_RESULTS, MPI_COMM_WORLD, &requests[i]);
    }
    MPI_Waitall(numMsgToRecv, requests, statuses);

  }

  MPI_Finalize();

  return 0;
}

/**
Fragment of the example output:

(3) Received from (2) tag 0
(3) Received from (0) tag 2
(3) Sending result 0 to 2
(3) Sending result 2 to 0
(5) Received from (4) tag 0
(5) Received from (2) tag 2
(5) Sending result 0 to 4
(5) Sending result 2 to 2

(0) Message: [1, 4, 3]
(2) Message: [3, 0, 5]
(4) Message: [5, 2, 1]

**/
