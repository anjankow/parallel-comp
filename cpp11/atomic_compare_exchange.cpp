#include <atomic>
#include <stdio.h>

int main(){
  std::atomic<int> a;
  std::atomic_store(&a, 10);
  int copy = a.load();
  std::atomic_store(&a, 6);
  printf("this is a copy: %i\n", copy);
  while(!std::atomic_compare_exchange_weak_explicit(
      &a,
      &copy,
      copy,
      std::memory_order_release,
      std::memory_order_relaxed)){
        printf("a = %i, copy = %i\n", a.load(), copy);
        // std::atomic_store(&a, a+1);
  }
  printf("END a = %i, copy = %i\n", a.load(), copy);
  return 0;
}
