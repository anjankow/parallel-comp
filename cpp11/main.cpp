#include <thread>
#include <vector>
#include <iostream>
#include <chrono>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <atomic>

using namespace std;
using namespace std::chrono_literals;


/**
	sample output for
	int num_threads = 5;
	int num_slots = 4;
	int repeats = 5;


	thread | rep | slot |    info
	 0   | (0) |      |  ENTER ---->   occupied slots: 1
	 0   | (0) |   0  |  slot
	 1   | (0) |      |  ENTER ---->   occupied slots: 3
	 3   | (0) |      |  ENTER ---->   occupied slots: 4
	 2   | (0) |      |  ENTER ---->   occupied slots: 2
	 2   | (0) |   3  |  slot
	 4   | (0) |      |  ENTER all slots occupied, waiting...
	 // now all the slots are taken and the remaining thread must wait for its turn
	 // start waiting on condition variable
	 3   | (0) |   2  |  slot
	 1   | (0) |   1  |  slot
	 // printing is a long process, that's why some prints show up after actual execution
	 0   | (0) |      |  LEAVE current occupied_slots: 3
	 2   | (0) |      |  LEAVE current occupied_slots: 2
	 4   | (0) |      |  ENTER signalled!
	 // condition variable signalled and condition occupied_slots < max is true,
	 3   | (0) |      |  LEAVE current occupied_slots: 3
	 3   | (1) |      |  ENTER ---->   occupied slots: 4
	 // next thread can enter
	 3   | (1) |   0  |  slot
	 0   | (1) |      |  ENTER ---->   occupied slots: 3
	 0   | (1) |   1  |  slot
**/

struct slot_indexer {
public:
	slot_indexer() {
		std::atomic_store(&current_index, 0);
	}

	// increase the slot index and return the current value
	int obtain_slot_index() {
		// atomic_fetch_add returns the value before, but it's fine, because we count from 0
		return std::atomic_fetch_add(&current_index, 1);
	}

	// decrease the current slot index value
	void release_slot_index() {
		std::atomic_fetch_sub(&current_index, 1);
	}

private:
	// current slot index
	std::atomic<int> current_index;
};

struct slot_allocator
{
	slot_allocator(int num_slots)
		: num_slots(num_slots)
	{
		std::atomic_store(&occupied_slots, 0);
	}

	// When there are still free slots, this function adds one to number of occupied slots
	// and lets the thread continue. Otherwise, this function blocks on condition variable waiting
	// for any thread to call leave().
	void enter(int n, int i)
	{
		// memory order acquire to read the current value
		if (std::atomic_load_explicit(&occupied_slots, std::memory_order_acquire) >= num_slots) {
			fancy_print(n, i, "ENTER all slots occupied, waiting...");
			std::unique_lock<std::mutex> lock(slots_mutex);

			cv.wait(lock, [&](){
				return std::atomic_load(&occupied_slots) < num_slots;
			});
			fancy_print(n, i, "ENTER signalled!");
		}

		// memory order release to update occupied_slots
		int previous_num = std::atomic_fetch_add_explicit(&occupied_slots, 1, std::memory_order_release);
		fancy_print(n, i, "ENTER ---->   occupied slots: " + std::to_string(previous_num + 1));
	}

	// When called, this function decreases number of occupied slots and signals
	// condition variable.
	void leave(int n, int i)
	{
		// memory order release to update occupied_slots
		int previous_num = std::atomic_fetch_sub_explicit(&occupied_slots, 1, std::memory_order_release);
		fancy_print(n, i, "LEAVE current occupied_slots: " + std::to_string(previous_num - 1));

		cv.notify_all();
	}

private:
	// max number of slots
	int num_slots;
	// keeps track on the number of occupied slots
	std::atomic<int> occupied_slots;
	// controlls the number of occupied slots
	std::mutex slots_mutex;
	// condition variable to signal that one slot is released now
	std::condition_variable cv;

	void fancy_print(int thread_idx, int repeat_idx, const std::string msg) {
			// first number is a thread index, then the repetition index, then nothing
			// (slot number is provided by slot indexer) and additional information
			printf("     %i   | (%i) |      |  %s\n", thread_idx, repeat_idx, msg.c_str());
	}
};

int main()
{
	int num_threads = 5;
	int num_slots = 4;
	int repeats = 5;
	slot_allocator alloc(num_slots);
	slot_indexer indexer;

	vector<thread> threads;
	auto t1 = chrono::high_resolution_clock::now();

	printf("  thread | rep | slot |    info\n");

	for (int t = 0; t < num_threads; ++t)
	{
		threads.push_back(thread([&](int num) {
			for (int r = 0; r < repeats; ++r)
			{
				alloc.enter(num, r);
				int index = indexer.obtain_slot_index();
				// first number is a thread index, then the repetition index, slot number got
				// from the slot indexer and additional information
				printf("     %i   | (%i) |   %i  |  slot\n", num, r, index);
				this_thread::sleep_for(1ms);
				indexer.release_slot_index();
				alloc.leave(num, r);
			}
		}, t));
	}
	for (auto& t : threads) t.join();
	auto t2 = chrono::high_resolution_clock::now();
	cout << chrono::duration<double>(t2 - t1).count() << endl;
}
