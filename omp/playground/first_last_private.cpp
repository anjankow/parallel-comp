#include "omp.h"
#include <cstdio>
#include <vector>

// FIRST PRIVATE - initialized with value from the master thread,
//                 each thread gets its own copy of the value
//
// OUTPUT:
//    thread (7), sum: 67
//    thread (7), sum: 68
//    thread (7), sum: 69
//    thread (7), sum: 70
//    thread (7), sum: 71
//    thread (3), sum: 67
//    thread (3), sum: 68
//    thread (3), sum: 69
//    thread (3), sum: 70
//    thread (3), sum: 71
//    After SUM: 66
//
// if added lastprivate(sum) to the omp statements, the output is:
//
//    thread (3), sum: 68
//    thread (3), sum: 69
//    thread (3), sum: 70
//    thread (3), sum: 71
//    thread (7), sum: 69
//    thread (7), sum: 70
//    thread (7), sum: 71
//    After SUM: 71

int main() {

  long int sum = 66;
  auto numbers = std::vector<int>(40, 1);

  #pragma omp parallel for firstprivate(sum)
  for (size_t i = 0; i < numbers.size(); ++i) {
    sum += numbers[i];
    printf("thread (%i), sum: %li\n", omp_get_thread_num(), sum);
  }

  printf("After SUM: %li\n", sum);
  return 0;
}
