#include "omp.h"
#include <cstdio>
#include <vector>


// OUTPUT
// variant 1:
// long int reducted = 0;
// long int notreducted = 0;
//
//    ...
//    thread (0), sum reducted: 1
//    thread (0), sum reducted: 2
//    thread (0), sum reducted: 6
//    thread (4), sum reducted: 13
//    thread (4), sum reducted: 182
//    ...
//    thread (1), sum not reducted: 0
//    thread (1), sum not reducted: 0
//    thread (1), sum not reducted: 0
//    thread (2), sum not reducted: 0
//    ...
//    REDUCTED: 0
//    NOT REDUCTED: 0
//
// Initial value of the reducted variable is always 1 for multiplication.
//
//
// variant 2:
// long int reducted = 1;
// long int notreducted = 1;
//
//    REDUCTED: 2432902008176640000
//    NOT REDUCTED: 243290200817664000
//
//    REDUCTED - NOT REDUCTED: 2189611807358976000
//
// Without reduction some loop iteration are lost in the final result.

int main() {

  long int reducted = 1;
  long int notreducted = 1;
  auto numbers = std::vector<int>(100);
  #pragma omp parallel for
  for (size_t i = 0; i < numbers.size(); ++i) {
    numbers[i] = i + 1;
  }

  #pragma omp parallel for reduction(+:reducted)
  for (int i = 0; i < 20; ++i) {
    reducted *= numbers[i];
    printf("thread (%i), sum reducted: %li\n", omp_get_thread_num(), reducted);
  }

  #pragma omp parallel for
  for (int i = 0; i < 20; ++i) {
    notreducted *= numbers[i];
    printf("thread (%i), sum not reducted: %li\n", omp_get_thread_num(), notreducted);
  }

  printf("\nREDUCTED: %li\nNOT REDUCTED: %li\n", reducted, notreducted);
  printf("\nREDUCTED - NOT REDUCTED: %li\n", reducted - notreducted);
  return 0;
}
