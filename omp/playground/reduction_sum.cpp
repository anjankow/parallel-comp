#include "omp.h"
#include <cstdio>
#include <vector>

int main() {

  int reducted = 0;
  int notreducted = 0;
  auto numbers = std::vector<int>(100);
  #pragma omp parallel for
  for (size_t i = 0; i < numbers.size(); ++i) {
    numbers[i] = i;
  }

  #pragma omp parallel for reduction(+:reducted)
  for (int i = 0; i < 20; ++i) {
    reducted += numbers[i];
    printf("thread (%i), sum reducted: %i\n", omp_get_thread_num(), reducted);
  }

  #pragma omp parallel for
  for (int i = 0; i < 20; ++i) {
    notreducted += numbers[i];
    printf("thread (%i), sum not reducted: %i\n", omp_get_thread_num(), notreducted);
  }

  printf("\nREDUCTED: %i\nNOT REDUCTED: %i\n", reducted, notreducted);
  return 0;
}
