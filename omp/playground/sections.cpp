#include "omp.h"
#include <cstdio>
#include <vector>

// ---- POSSIBLE OUTPUTS ----
//
// LAST thread (6), result: 0, init: 6
// thread (3), result: 0, init: 6
// thread (3), result: 6, init: 12
// thread (3), result: 6, init: 18
// ...
// thread (7), result: 6
// thread (7), result: 12
// thread (7), result: 18
// ...
// thread (7), result: 120
// FINAL thread (0), result: 120, init: 6
//
// ---- or ----
//
// LAST thread (1), result: 6, init: 6
// thread (7), result: 6
// ...
// thread (6), result: 120, init: 2172
// thread (6), result: 120, init: 2292
// FINAL thread (0), result: 120, init: 6
//
// Depending on which section is executed as first.

int main() {

  long int init = 6;
  long int result = 0;

  #pragma omp parallel
  {
    #pragma omp sections firstprivate(init)
    {
      #pragma omp section
      {
        for (int i = 0; i < 20; ++i) {
          result += init;
          printf("thread (%i), result: %li\n", omp_get_thread_num(), result);
        }
      }
      #pragma omp section
      {
        for (int i = 0; i < 20; ++i) {
          init += result;
          printf("thread (%i), result: %li, init: %li\n", omp_get_thread_num(), result, init);
        }
      }
      #pragma omp section
      {
        printf("LAST thread (%i), result: %li, init: %li\n", omp_get_thread_num(), result, init);
      }
    }
  }
  printf("FINAL thread (%i), result: %li, init: %li\n", omp_get_thread_num(), result, init);
  return 0;
}
