#include "omp.h"
#include <cstdio>
#include <vector>

// PRIVATE - unitialized
//
// OUTPUT:
//    thread (2), sum: 93979315061326
//    thread (2), sum: 93979315061361
//    thread (2), sum: 93979315061397
//    thread (2), sum: 93979315061434
//    thread (2), sum: 93979315061472
//    After SUM: 66

int main() {

  long int sum = 66;
  auto numbers = std::vector<int>(100);
  #pragma omp parallel for
  for (size_t i = 0; i < numbers.size(); ++i) {
    numbers[i] = i + 1;
  }

  #pragma omp parallel for private(sum)
  for (size_t i = 0; i < numbers.size(); ++i) {
    sum += i;
    printf("thread (%i), sum: %li\n", omp_get_thread_num(), sum);
  }

  printf("After SUM: %li\n", sum);
  return 0;
}
