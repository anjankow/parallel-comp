#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <chrono>
#include <omp.h>

using namespace std;

typedef vector<int> Array1D;
typedef vector<Array1D> Array2D;
typedef vector<Array2D> Image;

int colorStep;

// Test if point c belong to the Mandelbrot Set
bool mandelbrot(complex<double> c, vector<int> &pixel)
{

  int max_iteration = 1000, iteration = 0;
  complex<double> z(0, 0);

  while (abs(z) <= 4 && (iteration < max_iteration))
  {
    z = z * z + c;
    iteration++;
  }

  if (iteration != max_iteration)
  {
    pixel = {255, 255, 255}; // outside -> white 255,255,255
    return false;
  }

  int intesivity = omp_get_thread_num() * colorStep;

  pixel = {intesivity, intesivity, intesivity}; // inside -> grey

  return true;
}

int main(int argc, char **argv)
{
  colorStep = 255 / (omp_get_max_threads() + 1);

  // height and width of the output image
  // square size assumed for simplicity
  int width = 1200, height = 1200;

  int i, j, pixels_inside = 0;

  // Image data structure:
  // - for each pixel we need red, green, and blue values (0-255)
  // - we use 3 different matrices for corresponding channels
  const int channels = 3; // red, green, blue
  Image image(channels, Array2D(height, Array1D(width)));

  complex<double> c;

  // auto t1 = chrono::high_resolution_clock::now();
  auto t1 = omp_get_wtime(); // <-- use this time when you switch to OpenMP

  for (i = 0; i < height; i++)
  {
    #pragma omp parallel for reduction(+:pixels_inside) shared(image) schedule(static, 1)
    for (j = 0; j < width; j++)
    {

      // pixel to be passed to the mandelbrot function
      vector<int> pixel = {0, 0, 0}; // red,green,blue (each range 0-255)
      c = complex<double>(2.0 * ((double)j / width - 0.75), ((double)i / height - 0.5) * 2.0);

      if (mandelbrot(c, pixel))
      {
        pixels_inside++;
      }

      for (int ch = 0; ch < channels; ch++)
      {
        image[ch][i][j] = pixel[ch];
      }
    }
  }

  // auto t2 = chrono::high_resolution_clock::now();
  auto t2 = omp_get_wtime(); // <-- use this time when you switch to OpenMP

  cout << chrono::duration<double>(t2 - t1).count() << endl;

  return 0;
}
