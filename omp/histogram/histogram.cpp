#include <vector>
#include <iostream>
#include <random>
#include <chrono>
#include <numeric>
#include <omp.h>

using namespace std;

struct generator_config
{
	generator_config(int max) : max(max) {}
	int max;
};

struct generator
{
	generator(const generator_config &cfg) : dist(0, cfg.max) {}
	int operator()()
	{
		return dist(engine);
	}

private:
	minstd_rand engine;
	uniform_int_distribution<int> dist;
};

struct histogram
{
	histogram(int count) : data(count)
	{
	}
	void add(int i)
	{
#pragma omp atomic update
		++data[i];
	}
	void print(std::ostream &str)
	{
		for (size_t i = 0; i < data.size(); ++i)
			str << i << ":" << data[i] << endl;
		str << "total:" << accumulate(data.begin(), data.end(), 0) << endl;
	}

private:
	vector<int> data;
};

struct worker
{
	worker(int repeats_to_do, histogram &h, const generator_config &cfg) : repeats_to_do(repeats_to_do), h(h), cfg(cfg) {}
	void operator()()
	{
		thread_local generator gen(cfg);
#pragma omp parallel for default(none) shared(h) copyin(gen) num_threads(omp_get_max_threads()) schedule(dynamic, repeats_to_do / omp_get_max_threads())
		for (int i = 0; i < repeats_to_do; ++i)
		{
			int next = gen();
			h.add(next);
		}
	}

private:
	int repeats_to_do;
	histogram &h;
	const generator_config &cfg;
};

int main()
{
	int max = 10;
	int repeats_to_do = 500000000;

	generator_config cfg(max);
	histogram h(max + 1);

	auto t1 = omp_get_wtime();

	// do it in parallel with OpenMP
	// How and where you split the work depends on
	// what omp construct you want to use.
	// You can do it inside this worker function
	// or here in the main.
	// (uncommented worker function here is just one way to do it)

	worker(repeats_to_do, h, cfg)();

	auto t2 = omp_get_wtime();

	cout << chrono::duration<double>(t2 - t1).count() << endl;
}
