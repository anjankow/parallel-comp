#!/bin/bash

make clean
rm results*

make

for repeat in {1..10}
do
    echo "repeat $repeat"
    ./h.out >> results_h.csv
    tail -1 results_h.csv
    ./org.out >> results_org.csv
    tail -1 results_org.csv

done

